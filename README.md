# ci-tpl


### 🤔 Q+A


Q: What is this repo? <br>
A: A [gitlab components](https://docs.gitlab.com/ee/ci/components/#components-repository) repo with multiple CI components (mainly linters) maintained by me ([Aljaž Starc](https://aljaxus.eu)) for my job ([@flarenetwork](https://gitlab.com/flarenetwork/)) to make my (and maybe also your) life simpler.

Q: Why the weird docs URL? <br>
A: Because my account's username has a dot in it (my bad).
